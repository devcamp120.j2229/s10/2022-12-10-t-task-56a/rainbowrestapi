package com.devcamp.api.rainbow.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RainbowController {
    
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbows() {
        ArrayList<String> listRainbow = new ArrayList<String>();
        String[]rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

        // for (String rainbow : rainbows) {
        //     listRainbow.add(rainbow);
        // }
        listRainbow = new ArrayList<>(Arrays.asList(rainbows));

        /*
        listRainbow.add("red");
        listRainbow.add("orange");
        listRainbow.add("yellow"); */
        
        return listRainbow;
    }
}
